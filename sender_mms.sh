#!/bin/bash
. /Users/$USER/scripts/valid_ip.sh
# This script reads a video file in CIF format encoded with H264.

clear	# Clear the screen.

# Setting up the variables
OUTPUTDIR="/Users/$USER/Desktop/trabalho_multimidia/"
LOGFILE="$OUTPUTDIR/log/mmedia_system.log"
EVALVID="/Users/$USER/evalvid-2.7-mac/" 

echo "Multimedia Systems Homework Script."
echo "starting ..."

echo "Hi, $USER!"
echo "This script will follow you through the steps of Evalvid Tutorial"

if [ $# == 0 ]; then
	echo "To start, enter the CIF format video file path."
	echo "e.g '/Users/some_user/Desktop/filename.264'"
	PASS="false"
	while [ "$PASS" = "false" ]
	do
		echo "Enter Video File Path: "
		read FILEPATH
		FULLNAME=$(basename "$FILEPATH")
		EXTENSION="${FULLNAME##*.}"
		FILENAME="${FULLNAME%.*}"
		# Check if file exists and is a video with extension .264
		if [ ! -f $FILEPATH ]; then
			echo "Not a file. Enter an existing file path."
		elif [ "$EXTENSION" = "264" ]; then
			PASS="true"
		else
			echo "File does not have .264 extension."
		fi
	done
elif [ $# == 1 ]; then
	PASS="false"
	FILEPATH=$1
	FULLNAME=$(basename "$FILEPATH")
	EXTENSION="${FULLNAME##*.}"
	FILENAME="${FULLNAME%.*}"
	if [ ! -f $FILEPATH ]; then
		echo "Not a file. Enter an existing file path."
	elif [ "$EXTENSION" = "264"  ]; then
		PASS="true"
	else
		echo "File does not have .264 extension."
	fi
	while [ "$PASS" = "false" ]
	do
		echo "Enter Video File Path: "
		read FILEPATH
		FULLNAME=$(basename "$FILEPATH")
		EXTENSION="${FULLNAME##*.}"
		FILENAME="${FULLNAME%.*}"
		# Check if file exists and is a video wth extension .264
		if [ ! -f $FILEPATH ]; then
			echo "Not a file. Enter an existing file path."
		elif [ "$EXTENSION" = "264" ]; then
			PASS="true"
		else
			echo "File does not have .264 extension."
		fi
	done
else
	echo "More than one argument, only one is needed."
	exit 0
fi

# Make directory for output files if not exists
if [ ! -d $OUTPUTDIR ]; then
	mkdir $OUTPUTDIR
	mkdir $OUTPUTDIR/log
fi

# Decompress the input video file with 'ffmpeg'
echo "Decompressing input video file with 'ffmpeg'.:"
ffmpeg -i $FILEPATH $OUTPUTDIR$FILENAME.yuv
date >> $LOGFILE
echo "YUV file: '$OUTPUTDIR$FILENAME.yuv' created." >> $LOGFILE
echo

# Creates a compressed raw video with 30 fps, GOP length of 30 frames with no B-frames using 'ffmpeg'.
echo "Compressing CIF video using 'ffmpeg':"
ffmpeg -s cif -r 30 -b 64000 -bt 3200 -g 30 -i $OUTPUTDIR$FILENAME.yuv -vcodec mpeg4 $OUTPUTDIR$FILENAME.m4v
date >> $LOGFILE
echo "M4V file: '$OUTPUTDIR$FILENAME.m4v' created." >> $LOGFILE
echo

# MP4-Container
echo "Creating MP4-Container with 'MP4Box':"
MP4Box -hint -mtu 1024 -fps 30 -add $OUTPUTDIR$FILENAME.m4v $OUTPUTDIR$FILENAME.mp4
date >> $LOGFILE
echo "MP4-Container: '$OUTPUTDIR$FILENAME.mp4' created." >> $LOGFILE
echo

# Creates reference video
echo "Creating reference video with 'ffmpeg':"
ffmpeg -i $OUTPUTDIR$FILENAME.mp4 $OUTPUTDIR$FILENAME'_ref'.yuv
date >> $LOGFILE
echo "Reference video file: '$OUTPUTDIR$FILENAME'_ref.yuv' created." >> $LOGFILE
echo

# Extract the SDP-file with 'MP4Box' to send the file
echo "Extracting the SDF-file with 'MP4Box':"
MP4Box -std -sdp $OUTPUTDIR$FILENAME.mp4 > $OUTPUTDIR'temp'.sdp
date >> $LOGFILE
echo "SDP-file: '$OUTPUTDIR'temp.sdp' created." >> $LOGFILE

# Reads the IP of the destination, i.e. the receiver of the streamming
STAT="bad"
while [ "$STAT" = "bad" ]
do
	echo "Enter the IP of the receiver:"
	read IPDESTINATION
	valid_ip $IPDESTINATION
	if  [[ $? -eq 0 ]]; then
		if [ ! -z $IPDESTINATION ]; then # Check if the IP is reacheable
			echo "Checking if the ip is reacheable:"
			ping -c 1 $IPDESTINATION
			if [ $? -eq 0 ] ; then
				STAT="good"
				date >> $LOGFILE
				echo "$IPDESTINATION is reacheable." >> $LOGFILE
			else
				echo "Machine is not pinging."
			fi
		fi
	else
		echo "Invalid IP address!"
	fi
done

# Reads the Port of the destionation
#STAT="bad"
#while [ "$STAT" = "bad" ]
#do
	echo "Enter PORT of the destination hostname:"
	read PORTDEST
#	if telnet $IPDESTINATION $PORTDEST < /dev/null 2>&1 | grep -q Connected
#	then
#		STAT="good"
#		date >> $LOGFILE
#		echo "Port $PORTDEST is fine." >> $LOGFILE
#	else
#		echo "Port $PORTDEST is down!"
#	fi
#done

# Now we change the SDP-file with the PORT the destination is listening
echo "Changing the SDP-file:"
sed "s/video 0/video $PORTDEST/g" $OUTPUTDIR'temp.sdp' > $OUTPUTDIR$FILENAME.sdp
rm $OUTPUTDIR'temp.sdp' # Remove temp SDP-file
date >> $LOGFILE
echo "temp SDP-file removed and changed $FILENAME.sdp successfully." >> $LOGFILE
echo

# Sends hinted .mp4 file per RTP/UDP to a specified destination host using 'mp4trace'
echo "Sending  hinted .mp4:"
# $EVALVID'mp4trace' -t UDP -f -m cam -s $IPDESTINATION $PORTDEST $OUTPUTDIR$FILENAME.mp4 > $OUTPUTDIR'st_'$FILENAME
$EVALVID'mp4trace' -f -s $IPDESTINATION $PORTDEST $OUTPUTDIR$FILENAME.mp4 > $OUTPUTDIR'st_'$FILENAME
date >> $LOGFILE
echo "Sent file: $FILENAME.mp4 to host at $IPDESTINATION at port $PORTDEST." >> $LOGFILE
echo "File tracker: st_$FILENAME created."
echo

# in order to evaluate the transmission you have to trace IP packets at the sender using TCPDUMP
echo "Evaluating the transmission tracing the IP packets with 'tcpdump':"
sudo tcpdump -i en1 -n -tt -v udp port $PORTDEST > $OUTPUTDIR'sd_'$FILENAME
date >> $LOGFILE
echo "Sender evaluation file: sd_$FILENAME created." >> $LOGFILE

# Unsetting the variables
unset OUTPUTDIR LOGFILE PASS FILEPATH FULLNAME EXTENSION FILENAME

echo "I'm giving you back your prompt now!"
echo
