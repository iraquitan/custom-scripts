#!/bin/bash
FILE = $1

if [ ! -s "$FILE" ]; then
	echo "File $FILE does not exist."
fi
