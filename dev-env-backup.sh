#!/bin/bash
. ~/.bash_profile
# This script will backup the installed packages from Homebrew, Python, Ruby, npm and Bower
BACKUPDIR="/Users/$USER/Dropbox/Developer Environment/"

echo "Hi, $USER!"
echo
echo "This script will backup Homebrew, Python, Ruby, npm and Bower packages to Dropbox"
echo

# Backup Homebrew installed software
echo "Backing up Homebrew installed softwares..."
HOMEBREW="homebrew-backup.txt"
brew list | tee "$BACKUPDIR$HOMEBREW"
echo

# Backup Python pip packages
echo "Backing up Python global pip packages..."
PYTHON="python-pkg-backup.txt"
syspip freeze | tee "$BACKUPDIR$PYTHON"
echo

# Backup Ruby gems
echo "Backing up Ruby gems..."
RUBY="ruby-gems-backup.txt"
gem list | tee "$BACKUPDIR$RUBY"
echo

# Backup npm packages
echo "Backing up npm packages..."
NPM="npm-backup.txt"
npm list -g | tee "$BACKUPDIR$NPM"
echo

# Backup Bower packages
echo "Backing up Bower packages..."
BOWER="bower-backup.txt"
bower list | tee "$BACKUPDIR$BOWER"
